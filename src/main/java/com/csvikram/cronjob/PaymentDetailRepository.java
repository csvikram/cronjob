package com.csvikram.cronjob;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vikram.choudhary 07/05/21
 */

@Repository
public interface PaymentDetailRepository extends JpaRepository<PaymentDetail, Long> {}
