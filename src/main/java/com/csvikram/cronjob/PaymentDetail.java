package com.csvikram.cronjob;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author vikram.choudhary 07/05/21
 */

@Accessors(chain = true)
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class PaymentDetail {

	@Id
	@GeneratedValue
	private Long id;

	private String transactionId;
	private BigDecimal amount;

	@Enumerated(EnumType.STRING)
	private PaymentStatus paymentStatus;

	@CreationTimestamp
	private LocalDateTime createdOn;

	@UpdateTimestamp
	private LocalDateTime modifiedOn;

	public enum PaymentStatus {
		SUCCESS, FAILED, PENDING
	}

}