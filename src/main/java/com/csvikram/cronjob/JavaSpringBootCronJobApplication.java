package com.csvikram.cronjob;

import java.math.BigDecimal;
import java.util.UUID;

import com.csvikram.cronjob.PaymentDetail.PaymentStatus;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class JavaSpringBootCronJobApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSpringBootCronJobApplication.class, args);
	}

	@Bean
	CommandLineRunner init(PaymentDetailRepository paymentDetailRepository) {
		return args -> {
			for (int i = 0; i < 50; i++) {
				PaymentDetail paymentDetail = new PaymentDetail().setTransactionId(UUID.randomUUID().toString())
						.setAmount(BigDecimal.valueOf(Math.random() * 100)).setPaymentStatus(PaymentStatus.PENDING);
				paymentDetailRepository.save(paymentDetail);
			}
		};
	}
}
