package com.csvikram.cronjob;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.csvikram.cronjob.PaymentDetail.PaymentStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author vikram.choudhary 07/05/21
 */

@RequiredArgsConstructor
@Slf4j
@Component
public class CronJob {

	private static final String TASK_ID = "taskId";

	private static final ExecutorService executorService = Executors.newFixedThreadPool(
			Runtime.getRuntime().availableProcessors() * 5);

	private final PaymentDetailRepository paymentDetailRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Scheduled(cron = "0 * * * * ?")
	void enquirePaymentStatus() {

		try {
			long start = System.currentTimeMillis();
			String taskId = UUID.randomUUID().toString();
			MDC.put(TASK_ID, taskId);
			log.info("Starting payment status enquiry cron");

			int limit = 10; // limit can be as per the requirement
			AtomicInteger offset = new AtomicInteger(0);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();

			CriteriaQuery<PaymentDetail> cq = cb.createQuery(PaymentDetail.class);
			Root<PaymentDetail> paymentDetailRoot = cq.from(PaymentDetail.class);
			cq.where(cb.equal(paymentDetailRoot.get("paymentStatus"), PaymentStatus.PENDING));
			cq.select(paymentDetailRoot);
			cq.orderBy(cb.asc(paymentDetailRoot.get("id")));
			//Not using Pageable as offset will not be multiple of limit to get the page
			Map<PaymentStatus, Integer> paymentStatusWiseCount = new ConcurrentHashMap<>();
			int i = 1;
			while (true) {
				log.info("Limit: {}, offset: {}", limit, offset.get());
				TypedQuery<PaymentDetail> query = entityManager.createQuery(cq);
				List<PaymentDetail> paymentDetails = query.setMaxResults(limit).setFirstResult(offset.get())
						.getResultList();
				log.info("Records found for enquiry: {}", paymentDetails.size());
				if (paymentDetails.size() == 0) {
					break;
				}
				offset.addAndGet(limit);
				List<CompletableFuture<Void>> completableFutures = new ArrayList<>(paymentDetails.size());
				for (PaymentDetail paymentDetail : paymentDetails) {
					try {
						completableFutures.add(CompletableFuture.supplyAsync(() -> {
							try {
								MDC.put(TASK_ID, taskId + "_" + paymentDetail.getId());
								runPaymentStatusEnquiry(paymentDetail);
								if (!paymentDetail.getPaymentStatus().equals(PaymentStatus.PENDING))
									offset.decrementAndGet();
								return null;
							} catch (Exception e) {
								log.error("Error occurred while payment status enquiry: {}, error: {}",
										paymentDetail.getId(), e.getMessage());
								return null;
							} finally {
								paymentStatusWiseCount.merge(paymentDetail.getPaymentStatus(), 1, Integer::sum);
							}
						}, executorService));
					} catch (Exception e) {
						log.error("Unknown error occurred while  payment status enquiry: {}, error: {}",
								paymentDetail.getId(), e.getMessage());
					}
				}
				try {
					CompletableFuture.allOf(completableFutures.toArray(new CompletableFuture[0])).get();
				} catch (InterruptedException | ExecutionException e) {
					log.error("Error while getting data from the completable future", e);
				}
				log.info("Current paymentStatusWiseCount after loop no: {}, status: {}, record ids: {}", i,
						paymentStatusWiseCount,
						paymentDetails.stream().map(PaymentDetail::getId).collect(Collectors.toList()));
				if (paymentDetails.size() < limit)
					break;
				i++;
			}
			log.info("TotalTime taken to enquire payment status: {} ms", System.currentTimeMillis() - start);
		} finally {
			MDC.remove(TASK_ID);
		}
	}

	void runPaymentStatusEnquiry(PaymentDetail paymentDetail) {

		PaymentStatus paymentStatus = enquiryTransaction(paymentDetail.getTransactionId());
		log.info("Updated paymentStatus: {}", paymentStatus);
		paymentDetail.setPaymentStatus(paymentStatus);
		paymentDetailRepository.save(paymentDetail);
	}

	PaymentStatus enquiryTransaction(String transactionId) {
		// Here we are calling the third party service for getting latest payment status and updating the
		// paymentDetail. For testing purpose we will mock the response and return the paymentStatus SUCCESS, FAILED
		// and PENDING randomly

		log.info("Executing enquiry for transactionId: {}", transactionId);
		int randomInt = (int) (Math.random() * 3);
		if (randomInt == 0)
			return PaymentStatus.PENDING;
		if (randomInt == 1)
			return PaymentStatus.SUCCESS;
		return PaymentStatus.FAILED;
	}
}
